<?php

Route::get('/portals', 'PortalController@list_select')->name('portal.select');
Route::get('portal/user', 'PortalController@user_portal')->name('portal.user');
Route::put('portal/discount', 'PortalController@discount')->name('portal.discount');

Route::post('/product', 'ProductController@create')->name('product.create');
Route::put('/product/{product}', 'ProductController@update')->name('product.update');
Route::get('/products', 'ProductController@list')->name('product.list');
Route::get('/product/{product}', 'ProductController@show')->name('product.show');
Route::get('products/fields', 'ProductController@fields')->name('product.getFields');
Route::get('/product-select', 'ProductController@select')->name('product.select');
Route::post('refresh-products', 'ProductController@refreshProducts')->name('product.refresh');
Route::get('select-taxes', 'ProductController@select_taxes')->name('taxes.select');

Route::post('/field', 'FieldController@create')->name('field.create');
Route::put('/field/{field}', 'FieldController@update')->name('field.update');
Route::get('/fields', 'FieldController@list')->name('field.list');
Route::get('/field/{field}', 'FieldController@show')->name('field.show');

Route::post('/tax', 'TaxController@create')->name('tax.create');
Route::put('/tax/{tax}', 'TaxController@update')->name('tax.update');
Route::get('/taxes', 'TaxController@list')->name('tax.list');
Route::get('/tax/{tax}', 'TaxController@show')->name('tax.show');

Route::post('/bono', 'BonoController@create')->name('bono.create');
Route::put('/bono/{bono}', 'BonoController@update')->name('bono.update');
Route::get('/bonos', 'BonoController@list')->name('bono.list');
Route::get('/bono/{bono}', 'BonoController@show')->name('bono.show');

Route::post('/group', 'GroupController@create')->name('group.create');
Route::put('/group/{group}', 'GroupController@update')->name('group.update');
Route::get('/groups', 'GroupController@list')->name('group.list');
Route::get('/group/{group}', 'GroupController@show')->name('group.show');
Route::get('/group-select', 'GroupController@select')->name('group.select');

Route::post('/file/save', 'UploadController@store')->name('file.store');
Route::get('/file/show/{file}', 'UploadController@show')->name('file.show');
Route::post('/file/delete/{file}', 'UploadController@delete')->name('file.delete');

Route::get('/log-user', 'LogUserController@index')->name('log-user.index');
Route::get('/log-user/show/{log_user}', 'LogUserController@show')->name('log-user.show');

Route::get('/user', 'UserController@index')->name('user.index');
Route::post('/user', 'UserController@store')->name('user.store');
Route::put('/user/{user}', 'UserController@update')->name('user.update');
Route::get('/user/show/{user}', 'UserController@show')->name('user.show');
Route::get('/roles', 'UserController@roles')->name('user.roles');

Route::get('/role', 'RoleController@index')->name('role.index');
Route::post('/role', 'RoleController@store')->name('role.store');
Route::put('/role/{role}', 'RoleController@update')->name('role.update');
Route::get('/role/show/{role}', 'RoleController@show')->name('role.show');
Route::get('/permissions', 'RoleController@permissions')->name('role.permissions');

Route::get('user-permission', 'UserController@get_permission_user')->name('user_permission');
