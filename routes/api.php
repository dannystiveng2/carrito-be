<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login')->name('auth.login');
    Route::post('logout', 'AuthController@logout')->name('auth.logout');
    Route::post('register', 'AuthController@register')->name('auth.register');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('valid-token', 'AuthController@validToken');
    Route::post('restore', 'PasswordController@restore');
    Route::post('token', 'PasswordController@show');
    Route::post('reset', 'PasswordController@reset');
//    Route::post('me', 'AuthController@me');
});

Route::post('/login', 'LoginController@login')->name('user.login');
Route::post('/register', 'LoginController@register')->name('user.register');
Route::post('/refresh', 'LoginController@refresh')->name('user.refresh');
Route::post('/restore', 'LoginController@restore')->name('user.restore');
Route::post('/validate/{token}', 'LoginController@valid')->name('user.valid');
Route::post('/reset/{token}', 'LoginController@reset')->name('user.reset');

Route::post('/upload-file', 'UploadController@uploadFile')->name('storeFile');
Route::post('portal', 'PortalController@create')->name('portal.create');
Route::put('portal/{portal}', 'PortalController@update')->name('portal.update');
Route::post('verify-bonus/{cart}', 'BonoController@verify_bonus')->name('bono.verify');
Route::get('portal/discount', 'PortalController@discount')->name('portal.discount');

Route::post('shopping-pay', 'ShoppingController@shopping_pay')->name('shopping.pay');
Route::post('shopping-products', 'ShoppingController@store')->name('shopping.store');
Route::get('shopping/detail/{shopping}', 'ShoppingController@detail')->name('shopping.detail');

Route::get('custom-products', 'PortalController@custom_products')->name('portal.products');
Route::get('gatewayPayment', 'ShoppingController@gatewayPayment')->name('pagos');
