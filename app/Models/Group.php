<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    const NAME_SECTION = 'GROUP';
    protected $fillable = [
        'name','portal_id'
    ];

    public function logusers()
    {
        return $this->morphMany('App\Models\LogUser', 'loguserable');
    }

    public function product()
    {
        return $this->belongsToMany('App\Models\Product');
    }
}
