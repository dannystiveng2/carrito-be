<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShoppingProduct extends Model
{
    protected $table = 'shopping_product';
    protected $fillable = [
            'product_id', 'shopping_id',	'redeem_id', 'total', 'code_bono', 'value_bono', 'created_at',
            'updated_at', 'quantity',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
