<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const NAME_SECTION = 'ROL';

    protected $fillable = [
        'name', 'portal_id',
    ];

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission', 'role_permission');
    }

    public function logusers()
    {
        return $this->morphMany('App\Models\LogUser', 'loguserable');
    }
}
