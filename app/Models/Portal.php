<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portal extends Model
{
    protected $fillable = [
    'name',
    'product_url',
    'load_products',
    'custom_fields',
    'stock',
    'header_url',
    'token_external',
    'start_date_discount',
    'end_date_discount',
    'type_discount',
    'value_discount',
];

    public function fields()
    {
        return $this->hasMany('App\Models\Field');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User')->where('actived', true);
    }
}
