<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const NAME_SECTION = 'PRODUCT';
    protected $fillable = [
        'name',	'summary',	'price', 'available_stock',	'stock',	'origin_id',	'active_library',
            'image_id',	'portal_id', 'tax_id',	'created_manual',	'management',	'has_iva',	'iva',	'visible',    ];

    public function logusers()
    {
        return $this->morphMany('App\Models\LogUser', 'loguserable');
    }

    public function fields()
    {
        return $this->belongsToMany('App\Models\Field')->withPivot('value');
    }

    public function group()
    {
        return $this->belongsToMany('App\Models\Group');
    }

    public function file()
    {
        return $this->belongsTo('App\Models\File', 'image_id', 'id');
    }

    public function tax()
    {
        return $this->belongsTo('App\Models\Tax', 'tax_id', 'id');
    }
}
