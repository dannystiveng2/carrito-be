<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    const NAME_SECTION = 'TAX';
    protected $table = 'taxes';
    protected $fillable = ['type', 'value'];

    public function logusers()
    {
        return $this->morphMany('App\Models\LogUser', 'loguserable');
    }
}
