<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FieldProduct extends Model
{
    protected $table = 'field_product';
    protected $fillable = ['product_id', 'field_id', 'value'];
}
