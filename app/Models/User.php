<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;

    const NAME_SECTION = 'USUARIO';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'email_verified_at', 'password', 'uid',
        'role_id', 'personal_information_id', 'portal_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function logUSer()
    {
        return $this->hasMany('App\Models\LogUSer');
    }

    public function logusers()
    {
        return $this->morphMany('App\Models\LogUser', 'loguserable');
    }

    public function authorizeRoles($permissions)
    {
        abort_unless($this->hasAnyRole($permissions), 401);

        return true;
    }

    public function hasAnyRole($permissions)
    {
        if (is_array($permissions)) {
            foreach ($permissions as $permission) {
                if ($this->hasRole($permission)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($permissions)) {
                return true;
            }
        }

        return false;
    }

    public function hasRole($permission)
    {
        $has = $this->role->permissions()->where('name', $permission)->first();
        if ($has || $this->role->name == 'SuperAdmin') {
            return true;
        }

        return false;
    }
}
