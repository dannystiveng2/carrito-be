<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shopping extends Model
{
    const PAYMENT_STATUS_OPEN = 'Abierta';
    const PAYMENT_STATUS_REJECTED = 'Pago rechazado';
    const PAYMENT_STATUS_EFFECTIVE = 'Efectivas';
    const PAYMENT_STATUS_PENDIENT = 'Pago pendiente';
    const PAYMENT_METHOD_CARD = 'TC';
    const PAYMENT_METHOD_PAYMENTEZ = 'PSE';
    protected $table = 'shopping';
    protected $fillable = [
    'total', 'external_user_id', 'code_bono', 'external_user_name', 'paid_date', 'paid_at', 'paid_type', 'paid_status', 'token',
    'status',
    ];

    public function product()
    {
        return $this->belongsToMany('App\Models\Product', 'shopping_product');
    }

    public function shopping_product()
    {
        return $this->hasMany('App\Models\ShoppingProduct');
    }
}
