<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    const NAME_SECTION = 'FIELD';
    protected $fillable = [
        'name', 'portal_id', 'default_value',
    ];

    public function portal()
    {
        return $this->belongsTo('App\Models\Portal');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }

    public function logusers()
    {
        return $this->morphMany('App\Models\LogUser', 'loguserable');
    }
}
