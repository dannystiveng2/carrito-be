<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModulePermission extends Model
{
    protected $table = 'module_permissions';
    protected $fillable = ['name'];

    public function permissions()
    {
        return $this->hasMany('App\Models\Permission');
    }
}
