<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogUser extends Model
{
    const CREATE = 'CREAR';
    const EDIT = 'MODIFICAR';
    const DELETE = 'ELIMINAR';

    protected $fillable = [
        'user_id', 'action', 'section', 'object_name', 'initial_value', 'actual_value',
    ];

    public function loguserable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
