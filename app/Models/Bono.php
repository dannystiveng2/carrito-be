<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bono extends Model
{
    const NAME_SECTION = 'BONO';
    protected $table = 'bonos';
    protected $fillable = [
        'name', 'type', 'start_date', 'end_date', 'quantity', 'code_generic',
        'aplication', 'type_discount', 'value_discount', 'state', 'available_quantity',
        'group_id', 'product_id', 'all_products',
     ];

    public function logusers()
    {
        return $this->morphMany('App\Models\LogUser', 'loguserable');
    }

    public function redeems()
    {
        return $this->hasMany('App\Models\Redeem');
    }
}
