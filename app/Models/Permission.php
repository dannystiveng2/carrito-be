<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
        'name', 'module_permission_id', 'description',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Roles', 'role_permission');
    }
}
