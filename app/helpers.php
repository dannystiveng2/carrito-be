<?php

use App\Models\Bono;

if (!function_exists('tokenShopping')) {
    function tokenShopping()
    {
        $token = str_random(20);

        $validator = validator(['token' => $token], [
           'token' => 'required|unique:shopping,token',
       ]);

        if ($validator->fails()) {
            $token = tokenShopping();
        }

        return $token;
    }
}

if (!function_exists('getBono')) {
    function getBono($code_bono)
    {
        $bono = null;
        $bonoG = Bono::where('code_generic', $code_bono)
        ->where('state', true)
        ->first();

        $bonoA = Bono::join('redeems', 'redeems.bono_id', '=', 'bonos.id')
        ->where('bonos.state', true)
        ->where('redeems.state', 'PENDIENTE')
        ->where('redeems.code', $code_bono)
        ->first();

        if ($bonoG != null) {
            $bono = $bonoG;
        } else {
            $bono = $bonoA;
        }

        return $bono;
    }
}
