<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Shopping extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'total' => $this->total,
            'external_user_id' => $this->external_user_id,
            'external_user_name' => $this->external_user_name,
            'token' => $this->token,
            'id' => $this->id,
            'discount_portal' => $this->discount_portal,
            'discount_bono' => $this->discount_bono,
            'products' => ProductsCart::collection($this->shopping_product),
        ];
    }
}
