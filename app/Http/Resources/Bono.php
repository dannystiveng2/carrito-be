<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Bono extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
              'id' => $this->id,
               'name' => $this->name,
                'type' => $this->type,
                'start_date' => $this->start_date,
                'end_date' => $this->end_date,
                'quantity' => $this->quantity,
                'code_generic' => $this->code_generic,
                'type_discount' => $this->type_discount,
                'aplication' => $this->aplication,
                'value_discount' => $this->value_discount,
       ];
    }
}
