<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductsCart extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $product = $this->product;

        return [
            'name' => $product->name,
            'value_base' => $product->price,
            'iva' => $product->tax->value,
            'total' => $this->total,
            'code_bono' => $this->code_bono,
            'value_bono' => $this->value_bono,
            'quantity' => $this->quantity,
        ];
    }
}
