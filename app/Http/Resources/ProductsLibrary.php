<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductsLibrary extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $img = $this->file;

        $fielsProducts = [];
        foreach ($this->fields as $key => $value) {
            $fielsProducts[$value->name] = $value->pivot->value;
        }

        return [
            'name' => $this->name,
            'summary' => $this->summary,
            'price' => $this->price,
            'stock' => $this->stock,
            'available_stock' => $this->available_stock,
            'origin_id' => $this->origin_id,
            'image_url' => $img->path_file,
            'image_alt' => $img->summary,
            'has_iva' => $this->has_iva,
            'iva' => $this->tax->value,
        ] + $fielsProducts;
    }
}
