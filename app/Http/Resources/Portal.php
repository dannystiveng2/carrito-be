<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Portal extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'product_url' => $this->product_url,
            'stock' => $this->stock,
            'header_url' => $this->header_url,
        ];
    }
}
