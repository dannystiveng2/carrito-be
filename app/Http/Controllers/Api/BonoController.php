<?php

namespace App\Http\Controllers\Api;

use App\Models\Bono;
use App\Models\Product;
use App\Models\Shopping;
use Illuminate\Http\Request;

class BonoController extends ApiController
{
    public function verify_bonus(Shopping $cart, Request $request)
    {
        $validator = validator($request->all(), [
            'code_bono' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 422);
        }
        $bono = getBono($request->get('code_bono'));
        if ($bono == null) {
            return $this->errorResponse(['code_bono' => 'Codigo no encontrado y/o Codigo inactivo.'], 422);
        }

        //BUSCAR SI EL BONO APLICA PARA EL PRODUCTO
        $bonoProducts = [];
        switch ($bono->aplication) {
            case 'GROUP':
                $products = $cart->product;
                if ($products->count() > 0) {
                    foreach ($products as $key => $productBono) {
                        $product = Product::find($productBono->id)->group()->where('group_product.group_id', $bono->group_id)->first();
                        if ($product != null) {
                            array_push($bonoProducts, $productBono->id);
                        }
                    }
                }
                break;
            case 'PRODUCT':
                foreach ($cart->shopping_product as $key => $productBono) {
                    if ($productBono->product_id == $bono->product_id) {
                        array_push($bonoProducts, $productBono->product_id);
                    }
                }
                break;
            case 'ALL':
                foreach ($cart->shopping_product as $key => $productBono) {
                    array_push($bonoProducts, $productBono->product_id);
                }

                break;
        }

        return $this->showData([
            'products' => $bonoProducts,
            'token' => $cart->token,
            'code_bono' => $request->get('code_bono'),
            'type' => $bono->type,
            'start_date' => $bono->start_date,
            'end_date' => $bono->end_date,
            'type_discount' => $bono->type_discount,
            'value_discount' => $bono->value_discount,
        ], 201);
    }
}
