<?php

namespace App\Http\Controllers\Api;

use App\Models\Bono;
use App\Models\Portal;
use App\Models\Redeem;
use App\Models\Product;
use App\Models\Shopping;
use Illuminate\Http\Request;
use App\Models\ShoppingProduct;
use App\Http\Resources\Shopping as ShoppingResource;

class ShoppingController extends ApiController
{
    public function store(Request $request)
    {
        $token_external = $request->header('token-external');
        if (!$token_external) {
            return $this->errorResponse('token_external requerido', 422);
        } else {
            $portal = Portal::where('token_external', $token_external)->first();
            if (!$portal) {
                return $this->errorResponse('token_external invalido', 422);
            }
        }

        $validator = validator($request->all(), [
            'product_id' => ['required',  function ($attribute, $value, $fail) use ($portal) {
                if (Product::where(['id' => $value, 'portal_id' => $portal->id])->first() === null) {
                    $fail('El producto no pertenece a este portal');
                }
            }],
            'quantity' => ['required', function ($attribute, $value, $fail) use ($portal,$request) {
                $product = Product::where(['id' => $request->get('product_id'), 'portal_id' => $portal->id])->first();
                if ($product == null) {
                    $fail('Cantidad no disponible.');
                } elseif ($product->available_stock < $value) {
                    $fail('No hay cantidades disponibles del producto solicitado.');
                }
            }],
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 422);
        }

        $shoppingData = $request->all();
        $shopping = Shopping::find($request->get('shopping_id'));
        if ($shopping == null) {
            $shopping = new Shopping();
            $shoppingData['token'] = tokenShopping();
        }
        $shoppingData['state'] = 'BORRADOR';
        $shopping->fill($shoppingData);
        $shopping->save();

        if ($request->get('quantity') > 0) {
            $productShopping = ShoppingProduct::where(['product_id' => $request->get('product_id'), 'shopping_id' => $shopping->id])->first();
            if ($productShopping == null) {
                $productShopping = new ShoppingProduct();
            }
            $product = Product::find($request->get('product_id'));
            $valueProduct = $product->price * $request->get('quantity');
            if ($product->has_iva) {
                $valueProduct += $valueProduct * ($product->iva / 100);
            }

            $productShopping->fill(
                [
                    'product_id' => $request->get('product_id'),
                    'shopping_id' => $shopping->id,
                    'total' => $valueProduct,
                    'quantity' => $request->get('quantity'),
                ]
            );
            $productShopping->save();
        } else {
            $productShopping = ShoppingProduct::where(['product_id' => $request->get('product_id'), 'shopping_id' => $shopping->id])->first();
            if ($productShopping != null) {
                $productShopping->delete();
            }
        }

        //CALCULAR TOTAL COMPRA
        $totalCompra = ShoppingProduct::where('shopping_id', $shopping->id)->get()->sum('total');

        $shopping->total = $totalCompra;
        $shopping->save();

        return $this->showData(new ShoppingResource($shopping), 201);
    }

    public function shopping_pay(Request $request)
    {
        $token_external = $request->header('token-external');
        if (!$token_external) {
            return $this->errorResponse('token_external requerido', 422);
        } else {
            $portal = Portal::where('token_external', $token_external)->first();
            if (!$portal) {
                return $this->errorResponse('token_external invalido', 422);
            }
        }
        $validator = validator($request->all(), [
            'token_shopping' => 'required|exists:shopping,token',
            'external_user_id' => 'required',
            'external_user_name' => 'required',
            'code_bono' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 422);
        }

        $bono = getBono($request->get('code_bono'));
        if ($bono == null) {
            return $this->errorResponse(['code_bono' => 'Bono no disponible.'], 422);
        }

        $shoppingData = $request->all();

        $shopping = Shopping::where('token', $request->get('token_shopping'))->first();
        $shopping->fill($shoppingData);
        $shopping->save();

        //REDIMIR BONO
        $redeem = null;
        if ($request->get('code_bono') != '') {
            if ($bono->type == 'GENERAL') {
                $redeem = new Redeem();
                $redeem->fill([
                        'code' => $request->get('code_bono'),
                        'bono_id' => $bono->id,
                      //  'state' => 'USADO',
                    ]);
                $redeem->save();
            } else {
                $redeem = Redeem::where('code', $request->get('code_bono'))->first();
                $redeem->fill([
                       // 'state' => 'USADO',
                        ]);
                $redeem->save();
            }
        }
        //PRODUCTOS

        $discountBono = 0;
        foreach ($shopping->shopping_product as $key => $product) {
            if ($redeem != null) {
                $redeemData['redeem_id'] = $redeem->id;
                //VERIFICAR DESCUENTO DEL BONO

                if ($bono->type_discount == 'VALOR') {
                    $redeemData['value_bono'] = $bono->value_discount;
                } else {
                    $redeemData['value_bono'] = $product->total * ($bono->value_discount / 100);
                }

                $redeemData['code_bono'] = $request->get('code_bono');

                switch ($bono->aplication) {
                    case 'GROUP':

                        $product2 = Product::find($product->id)->group()->where('group_product.group_id', $bono->group_id)->first();
                        if ($product2 != null) {
                            $discountBono += $redeemData['value_bono'];
                            $product->fill($redeemData);
                        }

                        break;
                    case 'PRODUCT':
                            if ($product->product_id == $bono->product_id) {
                                $discountBono += $redeemData['value_bono'];
                                $product->fill($redeemData);
                            }
                        break;
                    case 'ALL':
                        $discountBono += $redeemData['value_bono'];
                        $product->fill($redeemData);

                        break;
                }

                $product->save();
            }
        }

        //DESCUENTO PORTAL
        if (date('Y-m-d H:i') >= $portal->start_date_discount && date('Y-m-d H:i') <= $portal->end_date_discount) {
            if ($portal->type_discount == 'VALOR') {
                $shopping->discount_portal = $portal->value_discount;
            } else {
                $shopping->discount_portal = $shopping->total * ($portal->value_discount / 100);
            }
        }

        $shopping->discount_bono = $discountBono;
        $shopping->save();

        return $this->showData(new ShoppingResource($shopping), 201);
    }

    public function detail(Shopping $shopping)
    {
        return $this->showData(new ShoppingResource($shopping), 201);
    }

    public function gatewayPayment(Request $request)
    {
        //DATOS PAGOS
        LogAction::create(['type' => 'pay', 'body' => json_encode($request->all())]);
        $dev_reference = $request->get('dev_reference', null);

        if ($dev_reference) {
            $shopping = Shopping::where('token', $dev_reference)->first();

            if ($shopping && $shopping->status != Shopping::PAYMENT_STATUS_EFFECTIVE) {
                $url = config('app.api_gateway_token_url');
                $curl = curl_init();
                $user = config('app.api_gateway_user');
                $pass = config('app.api_gateway_pass');

                $data = json_encode(array('user' => $user, 'pass' => $pass));
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_TIMEOUT, 3);
                $result = curl_exec($curl);

                curl_close($curl);
                $array = json_decode($result, true);

                if ($array && $array['token']) {
                    $headers = array(
                    'Content-Type: application/json',
                    'Authorization: Bearer '.$array['token'],
                );

                    $curl2 = curl_init();
                    $url = config('app.api_gateway_token_status_url').''.$dev_reference;

                    curl_setopt($curl2, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl2, CURLOPT_URL, $url);
                    curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl2, CURLOPT_TIMEOUT, 3);

                    $result2 = curl_exec($curl2);
                    $array2 = json_decode($result2, true);

                    if ($array2 && count($array2['data']) > 0 && $array2['data']['status']) {
                        switch ($array2['data']['status']) {
                            case 'APROBADO':
                                 $payment_status = Shopping::PAYMENT_STATUS_EFFECTIVE;
                                 break;

                            case 'RECHAZADO':
                                    $payment_status = Shopping::PAYMENT_STATUS_REJECTED;
                                    break;
                            default:
                                $payment_status = Shopping::PAYMENT_STATUS_PENDIENT;
                                 break;
                         }
                        switch ($array2['data']['payment_method']) {
                             case 'BANK_TRANSACTION':
                                 $payment_method = Shopping::PAYMENT_METHOD_PAYMENTEZ;
                                 break;
                             default:
                                $payment_method = Shopping::PAYMENT_METHOD_CARD;
                                 break;
                         }

                        
                        $fecha = $carbon = new Carbon($array2['data']['date_in']['date']);
                        $shopping->status = $payment_status;
                        $shopping->paid_type = $payment_method;
                        $shopping->authorization_code = $array2['data']['authorization_code'];
                        $shopping->paid_date = $fecha->toDateString();
                        $shopping->paid_at = $fecha->toDateTimeString();
                        $shopping->total = $array2['data']['amount'];
                        $shopping->save();

                        if ($payment_status == Shopping::PAYMENT_STATUS_EFFECTIVE) {
                            $array_mail = explode(',', config('app.array_mail'));

                            // Mail::to($array_mail)->send(new PaidInfo($shopping));
                            //event(new ShoppingEvent($shopping));
                        }

                        return response()->json(['success' => true, 'status' => $payment_status], 201);
                    }
                }
            } elseif ($shopping) {
                return response()->json(['success' => true, 'status' => $shopping->status], 201);
            }
        }

        return response()->json(['success' => false, 'mensaje' => 'Orden no encontrada'], 201);
    }
}
