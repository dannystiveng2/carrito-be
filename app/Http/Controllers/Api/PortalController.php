<?php

namespace App\Http\Controllers\Api;

use App\Models\Role;
use App\Models\User;
use App\Models\Portal;
use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Resources\ProductsLibrary;
use App\Http\Resources\Portal as PortalResource;

class PortalController extends ApiController
{
    public function create(Request $request)
    {
        $validator = validator($request->all(), [
            'name' => 'required|unique:portals,name',
            'load_products' => 'required|boolean',
            'custom_fields' => 'required|boolean',
            'product_url' => 'nullable|required_if:load_products,1|url',
            'header_url' => 'nullable|required_if:load_products,1',
            'stock' => 'required|boolean',
            'header_url' => 'required_if:change_products,1',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 422);
        }
        $info = $request->all();
        $info['token_external'] = str_random(20);

        $portal = new Portal();
        $portal->fill($info);
        $portal->save();

        $user = new User();
        $user->fill(
            [
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
                'role_id' => Role::where('name', 'Administrador')->first()->id,
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
                'portal_id' => $portal->id,
            ]
        );
        $user->save();

        return $this->showData(new PortalResource($portal), 201);
    }

    public function update(Request $request, Portal $portal)
    {
        $userId = $portal->user->id;
        $validator = validator($request->all(), [
            'email' => "required|email|unique:users,email,$userId",
            'password' => 'nullable',
            'custom_fields' => 'required|boolean',
            'stock' => 'required|boolean',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 422);
        }
        $info = $request->all();
        $portal->fill($info);
        $portal->save();

        if ($portal->user->email == $request->get('email')) {
            $user = User::find($portal->user->id);
            if ($request->get('password') != '') {
                $user->fill([
                    'password' => bcrypt($request->get('password')),
                ]);
            }
        } else {
            $userInactive = User::where('portal_id', $portal->id)->update(['actived' => false]);
            $user = new User();
            $user->fill(
            [
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
                'role_id' => Role::where('name', 'Administrador')->first()->id,
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
                  'portal_id' => $portal->id,
            ]
        );
        }
        $user->save();

        return $this->showData(new PortalResource($portal), 201);
    }

    public function discount(Request $request)
    {
        $token_external = $request->header('token-external');
        if (!$token_external) {
            return $this->errorResponse('token_external requerido', 422);
        } else {
            $portal = Portal::where('token_external', $token_external)->first();
            if (!$portal) {
                return $this->errorResponse('token_external invalido', 422);
            }
        }

        return $this->showData([
             'start_date_discount' => $portal->start_date_discount,
             'end_date_discount' => $portal->end_date_discount,
             'type_discount' => $portal->type_discount,
             'value_discount' => $portal->value_discount,
         ], 201);
    }

    public function custom_products(Request $request)
    {
        $token_external = $request->header('token-external');
        if (!$token_external) {
            return $this->errorResponse('token_external requerido', 422);
        } else {
            $portal = Portal::where('token_external', $token_external)->first();
            if (!$portal) {
                return $this->errorResponse('token_external invalido', 422);
            }
        }

        $products = Product::where('visible', true)->where('portal_id', $portal->id)->get();

        return $this->showData(ProductsLibrary::collection($products), 201);
    }
}
