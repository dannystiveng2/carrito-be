<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    /**
     * store resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeImage(Request $request)
    {
        $validator = validator($request->all(), [
            'qqfile' => 'required|image|max:10240',
            ]);
        if ($validator->passes()) {
            $imagen = $request->file('qqfile');
            $imagenStore = $request->file('qqfile')->store('public');
            $data = [];
            $data['success'] = true;
            $data['path'] = $imagenStore;
            $data['newUuid'] = $imagenStore;
            $data['image_path'] = Storage::url($imagenStore);

            return response()->json($data, 201);
        }

        return response()->json(['errors' => $validator->errors(), 'success' => false], 200);
    }

    /**
     * store resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeFile(Request $request)
    {
        ////if(!$request->ajax()) return redirect('/');

        $validator = validator($request->all(), [
            'qqfile' => 'required|file|mimetypes:application/pdf|max:20240',
            ]);
        if ($validator->passes()) {
            $imagen = $request->file('qqfile');
            $imagenStore = $request->file('qqfile')->store('public');
            $data = [];
            $data['success'] = true;
            $data['path'] = $imagenStore;
            $data['newUuid'] = $imagenStore;
            $data['file_path'] = Storage::url($imagenStore);

            return response()->json($data, 201);
        }

        return response()->json(['errors' => $validator->errors(), 'success' => false], 200);
    }

    /**
     * store resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $uid
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteFile(Request $request, $uid)
    {
        Storage::delete('public/'.$uid);
        $data = [];
        $data['success'] = true;

        return response()->json($data, 200);
    }

    /**
     * store resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeVideo(Request $request)
    {
        $validator = validator($request->all(), [
            'video' => 'required|file',
        ]);
        if ($validator->passes()) {
            $video = $request->file('video');
            $videoStore = $request->file('video')->store('public');

            $url = Storage::url($videoStore);

            return $this->showData([
                'url' => $url,
                'path' => $videoStore,
            ]);
        }

        return response()->json($validator->errors(), 422);
    }

    public function storeVideoAdmin(Request $request)
    {
        $validator = validator($request->all(), [
            'qqfile' => 'required|file',
        ]);
        if ($validator->passes()) {
            $video = $request->file('qqfile');
            $videoStore = $request->file('qqfile')->store('public');

            $url = Storage::url($videoStore);

            return response()->json([
                'success' => true,
                'url' => $url,
                'path' => $videoStore,
            ], 201);
        }

        return response()->json($validator->errors(), 422);
    }

    /**
     * store resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadFile(Request $request)
    {
        $validator = validator($request->all(), [
            'file' => 'required',
            ]);
        if ($validator->passes()) {
            $imagen = $request->file('file');
            $imagenStore = $request->file('file')->store('public');
            $data = [];
            $data['success'] = true;
            $data['path'] = $imagenStore;
            $data['newUuid'] = $imagenStore;
            $data['file_path'] = Storage::url($imagenStore);

            return response()->json($data, 201);
        }

        return response()->json(['errors' => $validator->errors(), 'success' => false], 200);
    }

    
}
