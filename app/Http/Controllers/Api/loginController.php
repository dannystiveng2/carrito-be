<?php

namespace App\Http\Controllers\Api;

use App\Role;
use App\User;
use App\HabeasData;
use App\Mail\Register;
use App\PasswordReset;
use GuzzleHttp\Client;
use App\Mail\ResetPassword;
use Illuminate\Support\Str;
use App\PersonalInformation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use App\Http\Resources\UserAuth;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\PersonalInformation as PersonalInformationResource;


class LoginController extends ApiController
{

    public function register(Request $request){

        $uid = $request->header('x-firebase-uid');
        $token = $request->header('Authorization');
        $token = trim($token, 'Bearer');
        
        $client = new Client();
        
        $response = $client->request('POST', config('app.url_verify_uid') , [
            'headers' => [
                'x-access-token' => $token
               ]
        ]);
       
        $response = json_decode($response->getBody()->getContents(),true);
      
        if ( !isset($response['uid']) || $response['uid']<> $uid) {
            return $this->showdata('error de autenticacion', 401);
        }
     
        $validator = validator($request->all(), [
            'email' => 'required|unique:users,email',
            'first_name' => 'required|string|max:20',
            'middle_name' => 'nullable|string|max:20',
            'last_name' => 'required|string|max:20',
            'second_last_name' => 'nullable|string|max:20',
            'commercial_information' => 'required',
            
        ]);
        
        if ($validator->fails()) {
            return $this->errorResponse([$validator->errors(),'message' => 'Credenciales invalidas'], 422);
        }

        $role = Role::where('name','Usuario')->firstOrFail();

        $personal_information= PersonalInformation::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'second_last_name' => $request->second_last_name,
            'email' => $request->email,
            'user_session' => 0,
            ]);
            
        $user =  User::create([
            'name' => $request->first_name,
            'email' => $request->email,
            'password' => bcrypt(env('pass_user')),
            'remember_token' => Str::random(10),
            'email_verified_at' => now(),
            'role_id' => $role->id,
            'personal_information_id' => $personal_information->id,
            'uid' => $uid,
        ]);
       
        $permissions=[
            ['type' => HabeasData::TERM, 'value' =>1],
            ['type' => HabeasData::COMMERCIAL_INFORMATION, 'value' => $request->commercial_information]
        ];

        foreach ($permissions as $key ) {
           
            $habeas = HabeasData::create([
            'user_id' => $user->id,
            'email' => $request->email,
            'type' => $key['type'],
            'value' => $key['value'],
            'ip_address'=> $this->getIp()
            ]);

        }

        Mail::to($request->email)->send(new Register($personal_information));
       
        return $this->showData(new UserAuth($user), 201);
    }

    public function login(Request $request){
        
        $token = $request->header('Authorization');
        $token = trim($token, 'Bearer');
        
        $client = new Client();
        
        $response = $client->request('POST', config('app.url_verify_uid') , [
            'headers' => [
                'x-access-token' => $token
               ]
        ]);

        $response = json_decode($response->getBody()->getContents(),true);
        
        if ( !isset($response['uid'])) {
            return $this->showdata('error de autenticacion', 401);
        }
        
        $validator = validator($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user || $user->uid <> $response['uid']) {
            return $this->errorResponse(['message' => 'Credenciales invalidas'], 401);
        }

        return $this->showData(new UserAuth($user), 201);

    }
    
    public function restore(Request $request){
        
        $validator = validator($request->all(), [
            'email' => 'required|exists:users',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        
        PasswordReset::where('email', $request->email)->delete();
        $token = Str::random(30);
        $password_reset = DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => now()
        ]);
        $password_reset = PasswordReset::where('email', $request->email)->first();
        
    
        $personal_information= PersonalInformation::where('email', $request->email)->first();
       
        $personal_information= new PersonalInformationResource($personal_information);
        
        Mail::to($request->email)->send(new ResetPassword($password_reset, $personal_information));

        $data=[
            'message' => 'El correo ha sido enviado conexito'
        ];

        return $this->showData($data, 201);
    }

    public function valid(Request $request, $token){
        $a=['token' => $token];
        $validator = validator($a, [
            'token' => 'required|exists:password_resets,token',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse(['message' => 'token invalido'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $restore = PasswordReset::where('token', $token)->firstOrFail();
        if ($restore->created_at < Carbon::now()->subDay(1)) {

            PasswordReset::where('token', $request->token)->delete();

            return $this->errorResponse('token vencido.', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $user = User::where('email', $restore->email)->first();
        $data=[
            'message' => 'token valido',
            'uid' => $user->uid,
            'email' => $user->email

        ];
        return $this->showData($data, 201);

    }

    public function reset(Request $request, $token)
    {
        $a=['token' => $token];
        $validator = validator($a, [
            'token' => 'required|exists:password_resets,token',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse(['message' => 'token invalido'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $restore = PasswordReset::where('token', $token)->first();
        if ($restore->created_at < Carbon::now()->subDay(1)) {
            PasswordReset::where('token', $token)->delete();

            return $this->errorResponse('token vencido.', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        PasswordReset::where('email', $restore->email)->delete();

        return $this->showData('Accion exitosa.', 201);
 
    }
    
    public function refresh()
    {
        $token = auth('api')->refresh(true, true);

        return $this->showData($token, 201);
    }

    public function getIp(){
        $ip = $_SERVER['REMOTE_ADDR'];
        if (getenv('HTTP_CLIENT_IP')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            $ip = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            $ip = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            $ip = getenv('HTTP_FORWARDED');
        }
        
          return $ip;
    }

}
