<?php

namespace App\Http\Controllers\Admin;

use App\Models\Portal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PortalController extends Controller
{
    public function list_select()
    {
        return Portal::get();
    }

    public function user_portal()
    {
        $user = Auth::user();

        return Portal::where('id', $user->portal_id)->first();
    }

    public function discount(Request $request)
    {
        $validator = validator($request->all(), [
            'value_discount' => 'nullable|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }

        $info = $request->all();
        $dateRange = $request->get('rangeDate');
        if (count($dateRange) > 0) {
            $info['start_date_discount'] = $dateRange[0];
            $info['end_date_discount'] = $dateRange[1];
        } else {
            $info['start_date_discount'] = null;
            $info['end_date_discount'] = null;
        }

        $portal = Portal::find($request->get('id'));
        $portal->fill($info);
        $portal->save();

        return response()->json($portal, 201);
    }
}
