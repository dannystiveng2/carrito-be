<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tax;
use App\Models\LogUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaxController extends Controller
{
    public function create(Request $request)
    {
        $info = $request->all();

        $validator = validator($info, [
            'type' => 'required',
            'value' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $tax = new Tax();
        $tax->fill($info);
        $tax->save();

        $this->create_log(LogUser::CREATE, Tax::NAME_SECTION, $tax, json_encode($tax));

        return response()->json($tax, 201);
    }

    public function update(Request $request, Tax $tax)
    {
        $info = $request->all();

        $validator = validator($info, [
              'type' => 'required',
            'value' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }

        $initial_value_product = json_encode($tax);

        $tax->fill($info);
        $tax->save();

        $actual_value = json_encode($tax);

        $this->create_log(LogUser::EDIT, Tax::NAME_SECTION, $tax, $actual_value, $initial_value_product);

        return response()->json($tax, 201);
    }

    public function list()
    {
        return Tax::paginate(15);
    }

    public function show(Tax $tax)
    {
        return response()->json($tax, 201);
    }
}
