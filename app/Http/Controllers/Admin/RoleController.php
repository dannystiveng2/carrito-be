<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\LogUser;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Models\ModulePermission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        if (Auth::user()->role->name == 'SuperAdmin') {
            $models = Role::whereNotIn('name', ['SuperAdmin', 'Usuario'])->orderBy('id', 'asc')->get();
        } else {
            $models = Role::whereNotIn('name', ['SuperAdmin', 'Administrador', 'Usuario'])->orderBy('id', 'asc')->get();
        }

        return  $models;
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        $validator = validator($request->all(), [
            'name' => 'required|string|max:190|unique:roles,name',

            'permissions_id' => 'required|array',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $role = new Role();
        $datos = $request->all();
        $datos['portal_id'] = $user->portal_id;
        $role->fill($datos);
        $role->save();

        $permissionsRole = array_filter($request->permissions_id, function ($elemento2) {
            return is_numeric($elemento2);
        });

        $role->permissions()->sync($permissionsRole);

        $actual_value = json_encode($role->load('permissions'));
        $action = LogUser::CREATE;
        $section = Role::NAME_SECTION;
        $object = $role;
        $this->create_log($action, $section, $object, $actual_value);

        return response()->json(['model' => $role, 'success' => true], 201);
    }

    public function update(Request $request, Role $role)
    {
        $validator = validator($request->all(), [
            'name' => 'required|string|max:190|unique:roles,name,'.$role->id,
            'permissions_id' => 'required|array',
            ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }

        $role1 = json_encode($role->load('permissions'));

        $role->fill($request->all());
        $role->save();

        $permissionsRole = array_filter($request->permissions_id, function ($elemento2) {
            return is_numeric($elemento2);
        });

        $role->permissions()->sync($permissionsRole);

        $initial_value = $role1;
        $actual_value = json_encode($role->load('permissions'));
        $action = LogUser::EDIT;
        $section = Role::NAME_SECTION;
        $object = $role;
        $this->create_log($action, $section, $object, $actual_value, $initial_value);

        return response()->json(['model' => $role, 'success' => true], 201);
    }

    public function show(Request $request, Role $role)
    {
        if (!in_array($role->name, ['SuperAdmin', 'Administrador', 'Usuario'])) {
            $permissions_id = [];
            foreach ($role->permissions as $key) {
                $permissions_id[] = $key->pivot->permission_id;
            }

            $role['permissions_id'] = $permissions_id;

            return $role;
        }

        return null;
    }

    public function permissions(Request $request)
    {
        $models = ModulePermission::with('permissions')->get();
        $module = [];
        foreach ($models as $key => $value) {
            $children = [];
            foreach ($value->permissions as $key_children => $value_children) {
                array_push($children, ['title' => $value_children['description'], 'key' => $value_children['id']]);
            }
            array_push($module, ['title' => $value['name'], 'key' => $value['name'], 'children' => $children]);
        }
        //$models = Permission::whereNotIn('name', ['roles', 'usuarios', 'detalleLogUsuario'])->orderBy('id', 'asc')->get();

        return  $module;
    }
}
