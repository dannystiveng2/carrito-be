<?php

namespace App\Http\Controllers\Admin;

use App\Models\Bono;
use App\Models\Redeem;
use App\Models\LogUser;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BonoController extends Controller
{
    public function create(Request $request)
    {
        $validator = validator($request->all(), [
            'name' => 'required',
            'type' => 'required',
            'rangeDate' => 'required|array',
            'quantity' => 'required|numeric',
            'code_generic' => 'nullable|required_if:type,GENERAL|unique:bonos,code_generic',
            'aplication' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }

        $user = Auth::user();
        $info = $request->all();
        $info['portal_id'] = $user->portal_id;
        $dateRange = $request->get('rangeDate');
        $info['start_date'] = $dateRange[0];
        $info['end_date'] = $dateRange[1];
        $info['available_quantity'] = $info['quantity'];
        switch ($request->get('aplication')) {
            case 'GROUP':
                $info['produc_id'] = null;
                $info['all_products'] = false;
                break;
            case 'PRODUCT':
                $info['group_id'] = null;
                $info['all_products'] = false;
                break;
            case 'ALL':
                $info['group_id'] = null;
                $info['produc_id'] = null;
                $info['all_products'] = true;
            break;
        }

        $bono = new Bono();
        $bono->fill($info);
        $bono->save();

        if ($request->get('type') == 'ALEATORIO') {
            //GENERANDO BONOS
            for ($i = 0; $i < $request->get('quantity'); ++$i) {
                $code = Str::random(10);
                //VERIFICAR SI EXISTE EL CODIGO
                $verifyCode = Redeem::where('code', $code)->first();
                if ($verifyCode != null) {
                    --$i;
                    break;
                } else {
                    $bonoAleatory = new Redeem();
                    $bonoAleatory->fill([
                        'code' => $code,
                        'bono_id' => $bono->id,
                        'state' => 'PENDIENTE',
                    ]);
                    $bonoAleatory->save();
                }
            }
        }

        //$this->create_log(LogUser::CREATE, Bono::NAME_SECTION, $bono, json_encode($bono));

        return response()->json($bono, 201);
    }

    public function update(Request $request, Bono $bono)
    {
        $init_quantity = $bono->quantity;
        $info = $request->all();

        $validator = validator($request->all(), [
            'rangeDate' => 'required|array',
            'quantity' => 'required|numeric|min:'.$bono->quantity,
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $dateRange = $request->get('rangeDate');
        DD($dateRange);
        $info['start_date'] = $dateRange[0];
        $info['end_date'] = $dateRange[1];

        $initial_value_product = json_encode($bono);

        $bono->quantity = $info['quantity'];
        $bono->start_date = $info['start_date'];
        $bono->end_date = $info['end_date'];
        $bono->save();

        if ($request->get('type') == 'ALEATORIO') {
            //GENERANDO BONOS
            for ($i = 0; $i < ($request->get('quantity') - $init_quantity); ++$i) {
                $code = Str::random(10);
                //VERIFICAR SI EXISTE EL CODIGO
                $verifyCode = Redeem::where('code', $code)->first();
                if ($verifyCode != null) {
                    --$i;
                    break;
                } else {
                    $bonoAleatory = new Redeem();
                    $bonoAleatory->fill([
                        'code' => $code,
                        'bono_id' => $bono->id,
                        'state' => 'PENDIENTE',
                    ]);
                    $bonoAleatory->save();
                }
            }
        }

        $actual_value = json_encode($bono);

        $this->create_log(LogUser::EDIT, Bono::NAME_SECTION, $bono, $actual_value, $initial_value_product);

        return response()->json($bono, 201);
    }

    public function list()
    {
        return Bono::paginate(15);
    }

    public function show(Bono $bono)
    {
        $bono->with('redeems');

        $bono->rangeDate2 = [$bono->start_date, $bono->end_date];

        $bono->redeems;

        return response()->json($bono, 201);
    }
}
