<?php

namespace App\Http\Controllers\Admin;

use App\Models\Group;
use App\Models\LogUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    public function create(Request $request)
    {
        $user = Auth::user();
        $info = $request->all();
        $info['portal_id'] = $user->portal_id;

        $validator = validator($info, [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $group = new Group();
        $group->fill($info);
        $group->save();

        $group->product()->sync($request->get('products'));

        $this->create_log(LogUser::CREATE, Group::NAME_SECTION, $group, json_encode($group));

        return response()->json($group, 201);
    }

    public function update(Request $request, Group $group)
    {
        $info = $request->all();

        $validator = validator($info, [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }

        $initial_value_product = json_encode($group);

        $group->fill($info);
        $group->save();

        $group->product()->sync($request->get('products'));

        $actual_value = json_encode($group);

        $this->create_log(LogUser::EDIT, Group::NAME_SECTION, $group, $actual_value, $initial_value_product);

        return response()->json($group, 201);
    }

    public function list()
    {
        return Group::paginate(15);
    }

    public function show(Group $group)
    {
        $group->products = $group->product->pluck('id');

        return response()->json($group, 201);
    }

    public function select()
    {
        return Group::get();
    }
}
