<?php

namespace App\Http\Controllers\Admin;

use App\Models\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UploadController extends Controller
{
    public function store(Request $request)
    {
        $rules = [
            'path' => ['required'],
        ];

        $validator = validator($request->all(), $rules, ['path.required' => 'El archivo es obligatorio.']);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $file = new File();
        if ($request->get('id') > 0) {
            $file = File::find($request->get('id'));
        }
        $file->fill($request->all());
        $file->save();

        return $file;
    }

    public function show(File $file)
    {
        return $file;
    }

    public function delete(File $file)
    {
        $file->delete();

        return response()->json(['success' => true], 201);
    }
}
