<?php

namespace App\Http\Controllers\Admin;

use App\Models\Field;
use App\Models\LogUser;
use App\Models\Product;
use App\Models\FieldProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FieldController extends Controller
{
    public function create(Request $request)
    {
        $user = Auth::user();
        $info = $request->all();
        $info['portal_id'] = $user->portal_id;
        $validator = validator($info, [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $field = new Field();
        $field->fill($info);
        $field->save();

        //CONSULTAR PRODUCTOS
        $products = Product::where('portal_id', $field->portal_id)->get();
        foreach ($products as $key => $value_product) {
            FieldProduct::create([
                'product_id' => $value_product->id,
                'field_id' => $field->id,
                'value' => $field->default_value,
            ]);
        }

        $this->create_log(LogUser::CREATE, Field::NAME_SECTION, $field, json_encode($field));

        return response()->json($field, 201);
    }

    public function update(Request $request, Field $field)
    {
        $info = $request->all();

        $validator = validator($info, [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }

        $initial_value_product = json_encode($field);

        $field->fill($info);
        $field->save();

        $actual_value = json_encode($field);

        $this->create_log(LogUser::EDIT, Field::NAME_SECTION, $field, $actual_value, $initial_value_product);

        return response()->json($field, 201);
    }

    public function list()
    {
        $user = Auth::user();
       
        return Field::with('portal')->paginate(15);
    }

    public function show(Field $field)
    {
        return response()->json($field, 201);
    }
}
