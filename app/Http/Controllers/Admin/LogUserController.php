<?php

namespace App\Http\Controllers\Admin;

use App\Models\LogUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LogUserController extends Controller
{
    public function index()
    {
        $models = LogUser::orderBy('id', 'desc')->get();
        foreach ($models as $key) {
            $key['user_id'] = $key->user;
            $key['date'] = $key->updated_at->format('d-m-y');
            $key['time'] = $key->updated_at->isoformat('H:mm');
        }

        $permission = Auth::user()->hasRole('detalleLogUsuario');

        return  ['models' => $models, 'permission' => $permission];
    }

    public function show(Request $request, LogUser $log_user)
    {
        return  $log_user;
    }
}
