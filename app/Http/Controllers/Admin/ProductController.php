<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tax;
use App\Models\File;
use App\Models\Field;
use App\Models\Portal;
use GuzzleHttp\Client;
use App\Models\LogUser;
use App\Models\Product;
use App\Models\FieldProduct;
use Illuminate\Http\Request;
use App\Models\ShoppingProduct;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function create(Request $request)
    {
        $user = Auth::user();
        $info = $request->all();
        $info['portal_id'] = $user->portal_id;
        $info['created_manual'] = true;

        $validator = validator($info, [
            'name' => 'required',
            'image_id' => 'required',
            'price' => 'required|numeric',
            'stock' => 'nullable|numeric',
            'has_iva' => 'required',
            'tax_id' => 'nullable|required_if:has_iva,1',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $info['available_stock'] = $info['stock'];
        $product = new Product();
        $product->fill($info);
        $product->save();

        $product->group()->sync($request->get('groups'));

        $fields = [];

        foreach ($request->get('customFields') as $key => $value) {
            if ($value != null) {
                $fields[$value['id']]['value'] = ((isset($value['value']))) ? $value['value'] : $value['default_value'];
                $fields[$value['id']]['created_at'] = date('Y-m-d H:i:s');
                $fields[$value['id']]['updated_at'] = date('Y-m-d H:i:s');
            }
        }
        $product->fields()->sync($fields);

        $this->create_log(LogUser::CREATE, Product::NAME_SECTION, $product, json_encode($product));

        return response()->json($product, 201);
    }

    public function update(Request $request, Product $product)
    {
        $info = $request->all();
        $info['slug'] = str_slug($info['name']);
        $validator = validator($info, [
           'name' => 'required',
            'image_id' => 'required',
            'price' => 'required|numeric',
            'stock' => 'nullable|numeric',
             'has_iva' => 'required',
            'tax_id' => 'nullable|required_if:has_iva,1',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $productsPay = ShoppingProduct::where('product_id', $product->id)->get()->sum('quantity');
        $info['available_stock'] = $info['stock'] - $productsPay;
        $initial_value_product = json_encode($product);

        $product->fill($info);
        $product->save();

        $product->group()->sync($request->get('groups'));

        $fields = [];

        foreach ($request->get('customFields') as $key => $value) {
            if ($value != null) {
                $fields[$value['id']]['value'] = ((isset($value['value']))) ? $value['value'] : $value['default_value'];
                $fields[$value['id']]['created_at'] = date('Y-m-d H:i:s');
                $fields[$value['id']]['updated_at'] = date('Y-m-d H:i:s');
            }
        }
        $product->fields()->sync($fields);

        $actual_value = json_encode($product);

        $this->create_log(LogUser::EDIT, Product::NAME_SECTION, $product, $actual_value, $initial_value_product);

        return response()->json($product, 201);
    }

    public function list()
    {
        return Product::paginate(15);
    }

    public function show(Product $product)
    {
        $product->groups = $product->group()->pluck('group_id');
        $product->fields = $product->fields;

        return response()->json($product, 201);
    }

    public function fields(Request $request)
    {
        $user = Auth::user();
        $portal = Portal::find($user->portal_id);
        $fields = $portal->fields;

        return response()->json($fields, 201);
    }

    public function select()
    {
        return Product::get();
    }

    public function refreshProducts(Request $request)
    {
        $user = Auth::user();
        $client = new Client();

        $response = $client->request('GET', config('app.url_product_library'), [
            'headers' => [
                'Authorization' => 'application/json',
                'Authorization' => config('app.token_api_library'),
               ],
        ]);

        $response = json_decode($response->getBody()->getContents(), true);

        $ids_encontrados = [];
        $data = [];

        foreach ($response['data'] as $value) {
           
            if (isset($value['imagen']) && $value['imagen'] != '') {
                array_push($ids_encontrados, $value['_id']);

                $product = Product::where('origin_id', $value['_id'])->where('portal_id', $user->portal_id)->first();

                $value['name'] = $value['nombre'];
                $value['summary'] = $value['descripcion'];

                if ($product == null) {
                    $product = new Product();
                    $value['origin_id'] = $value['_id'];
                    $value['type'] = 'new';
                } else {
                    $value['type'] = 'edit';
                    $initial_value = json_encode($product->load('file', 'fields'));
                }

                $value['active_library'] = true;
                $value['created_manual'] = false;
                $value['portal_id'] = $user->portal_id;
                $product->fill($value);
                $product->save();

                //BUSCAR CAMPOS EXTRAER DEL SERVICIO CONFIGURADO POR EL PORTAL
                foreach (Field::where(['portal_id' => $user->portal_id])->get() as $key => $field) {
                    $fieldProduct = FieldProduct::where(['product_id' => $product->id, 'field_id' => $field->id])->first();
                    if ($fieldProduct == null) {
                        $fieldProduct = new FieldProduct();
                    }

                    $fieldProduct->fill([
                        'product_id' => $product->id,
                         'field_id' => $field->id,
                          'value' => ((isset($value[$field['name']])) ? $value[$field['name']] : $field->value_default),
                    ]);
                    $fieldProduct->save();
                }

                //CREATE / UPDATE IMAGE

                $image = [
                'path_file' => $value['imagen'],
                'path' => $value['imagen'],
                'type' => 'image',
            ];

                $imageProduct = $product->file;
                if ($imageProduct == null) {
                    $imageProduct = new File();
                }
                $imageProduct->fill($image);
                $imageProduct->save();
                $product->image_id = $imageProduct->id;
                $product->save();

                $actual_value = json_encode($product->load('file', 'fields'));

                if ($value['type'] == 'new') {
                    $initial_value = null;
                    $action = LogUser::CREATE;
                } elseif ($value['type'] == 'edit') {
                    $action = LogUser::EDIT;
                }

                $this->create_log($action, Product::NAME_SECTION, $product, $actual_value, $initial_value);
            }
        }

        //VERIFICAR Y DESABILITAR LOS PRODUCTOS QUE SE ELIMINAN DESDE LIBRERIA
        $productActive = Product::select('*')
        ->where('portal_id', $user->portal_id)
        ->whereNotIn('products.origin_id', $ids_encontrados)
        ->get();
        foreach ($productActive as $key => $value) {
            $product = Product::find($value->id);

            $initial_value = json_encode($product->load('file'));
            $product->update(['active_library' => false]);

            $this->create_log(LogUser::EDIT, Product::NAME_SECTION, $product, json_encode($product->load('file')), $initial_value);
        }

        $products = Product::where('portal_id', $user->portal_id)->select('*', 'id as key')->get();

        return response()->json($products, 201);
    }

    public function select_taxes()
    {
        return Tax::where('type', 'IVA')->get();
    }
}
