<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use App\Models\LogUser;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user()->load('role');

        $models = User::where('portal_id', $user->portal_id)->with('role')->get();

        return $models;
    }

    public function store(Request $request)
    {
        $userS = Auth::user();
        $validator = validator($request->all(), [
            'name' => 'required|string|max:190',
            'email' => 'required|email|unique:users,email',
            'role' => 'required',
            'password' => 'required|string|min:6|max:12',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $user = new User();
        $datos = $request->all();
        $datos['password'] = bcrypt($request->password);
        $datos['role_id'] = $request->role['id'];
        $datos['portal_id'] = $userS->portal_id;
        $datos['email_verified_at'] = now();
        $datos['remember_token'] = Str::random(10);
        $user->fill($datos);
        $user->save();

        $actual_value = json_encode($user->load('role'));
        $action = LogUser::CREATE;
        $section = User::NAME_SECTION;
        $object = $user;
        $this->create_log($action, $section, $object, $actual_value);

        return response()->json(['model' => $user, 'success' => true], 201);
    }

    public function update(Request $request, User $user)
    {
        $validator = validator($request->all(), [
            'name' => 'required|string|max:190',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'role' => 'required',
            'password' => 'nullable|string|min:6|max:12',
            ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }

        $user1 = json_encode($user->load('role'));

        $datos = $request->all();
        if ($request->password) {
            $datos['password'] = bcrypt($request->password);
        }
        $datos['role_id'] = $request->role['id'];
        $user->fill($datos);
        $user->save();

        $initial_value = $user1;
        $actual_value = json_encode($user->load('role'));
        $action = LogUser::EDIT;
        $section = User::NAME_SECTION;
        $object = $user;
        $this->create_log($action, $section, $object, $actual_value, $initial_value);

        return response()->json(['model' => $user, 'success' => true], 201);
    }

    public function show(Request $request, User $user)
    {
        $user['role'] = $user->role;

        return  $user;
    }

    public function roles(Request $request)
    {
        if (Auth::user()->role->name == 'SuperAdmin') {
            $models = Role::whereNotIn('name', ['SuperAdmin', 'Usuario'])->orderBy('id', 'asc')->get();
        } else {
            $models = Role::whereNotIn('name', ['SuperAdmin', 'Administrador', 'Usuario'])->orderBy('id', 'asc')->get();
        }

        return  $models;
    }

    public function get_permission_user()
    {
        $permissions = [];
        $user = Auth::user();
        foreach ($user->role->permissions()->get() as $key => $value) {
            $permissions[$value->name] = true;
        }

        return $permissions;
    }
}
