<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait LogUserT
{
    protected function create_log($action, $section, $object, $actual_value, $initial_value = null)
    {
        $user = Auth::user();
        $log = $object->logusers()->create([
            'user_id' => $user->id,
            'action' => $action,
            'section' => $section,
            'object_name' => ((isset($object['name'])) ? $object['name'] : ''),
            'initial_value' => $initial_value,
            'actual_value' => $actual_value,
        ]);
    }
}
