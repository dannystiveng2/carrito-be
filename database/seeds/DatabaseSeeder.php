<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        $rolAdmin = Role::create(['name' => 'Administrador']);
        $rolUser = Role::create(['name' => 'Usuario']);

        $userAdmin = User::create([
              'name' => 'admin@gmail.com',
              'email' => 'admin@gmail.com',
              'password' => bcrypt('Colombia2019*'),
              'role_id' => $rolAdmin->id,
              'email_verified_at' => now(),
              'remember_token' => Str::random(10)
          ]);
        $userUser = User::create([
              'name' => 'user@gmail.com',
              'email' => 'user@gmail.com',
              'password' => bcrypt('Colombia2019*'),
              'role_id' => $rolUser->id,
              'email_verified_at' => now(),
              'remember_token' => Str::random(10)
          ]);
    }
}
