<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('shopping', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('total')->comment('Total de la compra');
            $table->double('discount_portal')->comment('Total de descuento para el portal');
            $table->double('discount_bono')->comment('Total bonos aplicados');
            $table->integer('external_user_id')->nullable()->comment('Usuario quien realiza la compra');
            $table->integer('external_user_name')->nullable()->comment('Usuario quien realiza la compra');
            $table->date('paid_date')->nullable();
            $table->string('code_bono', 100)->nullable()->comment('Codigo del Bono');
            $table->timestamp('paid_at')->nullable();
            $table->string('paid_type')->nullable();
            $table->string('paid_status')->nullable();
            $table->string('authorization_code')->nullable();

            $table->string('token', 250)->nullable()->comment('mac_address + navegador');
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('shopping');
    }
}
