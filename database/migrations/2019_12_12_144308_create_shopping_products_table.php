<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingProductsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('shopping_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('shopping_id');
            $table->unsignedBigInteger('redeem_id')->nullable();
            $table->double('total')->comment('Total del producto');
            $table->string('code_bono', 100)->nullable()->comment('Codigo del Bono');
            $table->double('value_bono', 100)->nullable()->comment('Valor total del bono');
            $table->integer('quantity')->comment('Catidad de productos');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('shopping_id')->references('id')->on('shopping');
            $table->foreign('redeem_id')->references('id')->on('redeems');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('shopping_product');
    }
}
