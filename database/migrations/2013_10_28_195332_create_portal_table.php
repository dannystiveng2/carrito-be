<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortalTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('portals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('nombre');
            $table->string('product_url')->nullable()->comment('Url externa para traer los productos.');
            $table->boolean('load_products', false)->default(false);
            $table->boolean('custom_fields', false)->default(false);
            $table->boolean('stock', false)->default(false);
            $table->text('header_url')->nullable();
            $table->text('token_external')->nullable();

            $table->timestamp('start_date_discount')->nullable();
            $table->timestamp('end_date_discount')->nullable();
            $table->enum('type_discount', ['PORCENTAJE', 'VALOR'])->nullable();
            $table->double('value_discount')->comment('Valor del descuento a aplicar')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('portals');
    }
}
