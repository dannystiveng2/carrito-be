<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonosTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('bonos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('type', ['GENERAL', 'ALEATORIO']);
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->integer('quantity')->comment('Catidad de bonos a generar');
            $table->integer('available_quantity')->comment('Catidad de bonos disponible');
            
            $table->string('code_generic', 100)->nullable();
            $table->enum('aplication', ['GROUP', 'PRODUCT', 'ALL'])->comment('A quien se aplicara el bono.');
            $table->enum('type_discount', ['PORCENTAJE', 'VALOR']);
            $table->double('value_discount')->comment('Valor del descuento a aplicar');
            $table->boolean('state')->default(false);
            $table->unsignedBigInteger('group_id')->nullable();
            $table->unsignedBigInteger('product_id')->nullable();
            $table->boolean('all_products')->default(false);

            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('product_id')->references('id')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('bonos');
    }
}
