<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('Nombre');
            $table->string('email')->comment('Correo Electronico')->unique();
            $table->timestamp('email_verified_at')->comment('Fecha Correo verificado')->nullable();
            $table->string('password')->comment('Contraseña');
            $table->string('uid')->comment('uid')->nullable();
            $table->unsignedBigInteger('role_id')->comment('Rol');
            $table->boolean('actived')->default(true);
            $table->foreign('role_id')->references('id')->on('roles');
            $table->unsignedBigInteger('portal_id')->comment('Portal Asociado');

            $table->foreign('portal_id')->references('id')->on('portals');
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
