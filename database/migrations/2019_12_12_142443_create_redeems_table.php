<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedeemsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('redeems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->comment('Codigo del bono a redimir.');
            $table->unsignedBigInteger('bono_id');
            $table->enum('state', ['PENDIENTE', 'USADO'])->default('PENDIENTE');
            $table->foreign('bono_id')->references('id')->on('bonos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('redeems');
    }
}
