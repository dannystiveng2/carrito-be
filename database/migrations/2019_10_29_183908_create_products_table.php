<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('nombre');
            $table->text('summary')->nullable()->comment('resumen');
            $table->double('price')->nullable()->comment('precio');

            $table->integer('stock')->comment('Cantidad stock');
            $table->integer('available_stock')->comment('Cantidad de stock disponible');

            $table->string('origin_id');
            $table->integer('active_library')->default(false);
            $table->unsignedBigInteger('image_id')->nullable()->comment('imagen del servicio');
            $table->unsignedBigInteger('portal_id')->comment('portal');

            $table->boolean('created_manual')->default(false);
            $table->boolean('management', false)->default(false);
            $table->boolean('has_iva', false)->default(false);
            $table->unsignedBigInteger('tax_id')->nullable()->comment('impuesto');

            $table->boolean('visible', false)->default(false);
            $table->foreign('tax_id')->references('id')->on('taxes');
            $table->foreign('image_id')->references('id')->on('files');
            $table->foreign('portal_id')->references('id')->on('portals');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
