<?php

use App\Models\Role;
use App\Models\Permission;
use App\Models\ModulePermission;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description')->comment('Nombre Permiso');
            $table->string('name')->comment('Nombre Permiso')->unique();
            $table->unsignedBigInteger('module_permission_id')->nullable();
            $table->foreign('module_permission_id')->references('id')->on('module_permissions');
            $table->timestamps();
        });
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('Nombre Rol')->unique();
            $table->unsignedBigInteger('portal_id')->nullable();
            $table->foreign('portal_id')->references('id')->on('portals');
            $table->timestamps();
        });

        Schema::create('role_permission', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('permission_id');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('permission_id')->references('id')->on('permissions');
            $table->unique(['role_id', 'permission_id']);
        });

        $r2 = Role::create(['name' => 'Administrador']);

        $moduleUser = ModulePermission::create(['name' => 'Usuarios']);
        Permission::create(['description' => 'Listado', 'name' => 'users.list', 'module_permission_id' => $moduleUser->id]);
        Permission::create(['description' => 'Crear', 'name' => 'users.create', 'module_permission_id' => $moduleUser->id]);
        Permission::create(['description' => 'Ver Detalle', 'name' => 'users.view', 'module_permission_id' => $moduleUser->id]);
        Permission::create(['description' => 'Editar', 'name' => 'users.edit', 'module_permission_id' => $moduleUser->id]);

        $moduleRoles = ModulePermission::create(['name' => 'Roles']);
        Permission::create(['description' => 'Listado', 'name' => 'roles.list', 'module_permission_id' => $moduleRoles->id]);
        Permission::create(['description' => 'Crear', 'name' => 'roles.create', 'module_permission_id' => $moduleRoles->id]);
        Permission::create(['description' => 'Ver Detalle', 'name' => 'roles.view', 'module_permission_id' => $moduleRoles->id]);
        Permission::create(['description' => 'Editar', 'name' => 'roles.edit', 'module_permission_id' => $moduleRoles->id]);

        $moduleDescuentos = ModulePermission::create(['name' => 'Descuentos Portal']);
        Permission::create(['description' => 'Descuento Editar', 'name' => 'portal.discount', 'module_permission_id' => $moduleDescuentos->id]);

        $moduleImpuesto = ModulePermission::create(['name' => 'Impuestos']);
        Permission::create(['description' => 'Listado', 'name' => 'taxes.list', 'module_permission_id' => $moduleImpuesto->id]);
        Permission::create(['description' => 'Crear', 'name' => 'taxes.create', 'module_permission_id' => $moduleImpuesto->id]);
        Permission::create(['description' => 'Ver Detalle', 'name' => 'taxes.view', 'module_permission_id' => $moduleImpuesto->id]);
        Permission::create(['description' => 'Editar', 'name' => 'taxes.edit', 'module_permission_id' => $moduleImpuesto->id]);

        $moduleFields = ModulePermission::create(['name' => 'Campos']);
        Permission::create(['description' => 'Listado', 'name' => 'fields.list', 'module_permission_id' => $moduleFields->id]);
        Permission::create(['description' => 'Crear', 'name' => 'fields.create', 'module_permission_id' => $moduleFields->id]);
        Permission::create(['description' => 'Ver Detalle', 'name' => 'fields.view', 'module_permission_id' => $moduleFields->id]);
        Permission::create(['description' => 'Editar', 'name' => 'fields.edit', 'module_permission_id' => $moduleFields->id]);

        $moduleGroups = ModulePermission::create(['name' => 'Grupos']);
        Permission::create(['description' => 'Listado', 'name' => 'groups.list', 'module_permission_id' => $moduleGroups->id]);
        Permission::create(['description' => 'Crear', 'name' => 'groups.create', 'module_permission_id' => $moduleGroups->id]);
        Permission::create(['description' => 'Ver Detalle', 'name' => 'groups.view', 'module_permission_id' => $moduleGroups->id]);
        Permission::create(['description' => 'Editar', 'name' => 'groups.edit', 'module_permission_id' => $moduleGroups->id]);

        $moduleProducts = ModulePermission::create(['name' => 'Productos']);
        Permission::create(['description' => 'Listado', 'name' => 'products.list', 'module_permission_id' => $moduleProducts->id]);
        Permission::create(['description' => 'Crear', 'name' => 'products.create', 'module_permission_id' => $moduleProducts->id]);
        Permission::create(['description' => 'Ver Detalle', 'name' => 'products.view', 'module_permission_id' => $moduleProducts->id]);
        Permission::create(['description' => 'Editar', 'name' => 'products.edit', 'module_permission_id' => $moduleProducts->id]);

        $moduleBonos = ModulePermission::create(['name' => 'Bonos']);
        Permission::create(['description' => 'Listado', 'name' => 'bonos.list', 'module_permission_id' => $moduleBonos->id]);
        Permission::create(['description' => 'Crear', 'name' => 'bonos.create', 'module_permission_id' => $moduleBonos->id]);
        Permission::create(['description' => 'Ver Detalle', 'name' => 'bonos.view', 'module_permission_id' => $moduleBonos->id]);
        Permission::create(['description' => 'Editar', 'name' => 'bonos.edit', 'module_permission_id' => $moduleBonos->id]);

        $moduleLog = ModulePermission::create(['name' => 'Log Usuarios']);
        Permission::create(['description' => 'Listado', 'name' => 'log-user', 'module_permission_id' => $moduleLog->id]);
        Permission::create(['description' => 'Crear', 'name' => 'log-user-show', 'module_permission_id' => $moduleLog->id]);

        $permissions = Permission::get()->pluck('id')->toArray();
        $r2->permissions()->sync($permissions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_permission');
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('roles');
    }
}
