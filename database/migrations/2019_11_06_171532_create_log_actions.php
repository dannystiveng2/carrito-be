<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogActions extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('log_actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->comments('tipo');
            $table->text('body')->nullable()->comments('descripcion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('log_actions');
    }
}
