<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex, nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Gestion Carrito </title>
    <link href="{{ asset('css/template.css')}}" rel="stylesheet">
</head>


<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <div id="app">
        <header class="app-header navbar">
            <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand text-center" href="#"> <strong>Carrito</strong> </a>
            <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item">
                    <div>
                        <a class="btn btn-primary" href="{{ url('/') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><i class="fa fa-lock"></i>
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </header>

        <div class="app-body">

            @include('template.sidebar')
            <!-- Contenido Principal -->
            @yield('content')
            <!-- /Fin del contenido principal -->
        </div>
    </div>
    <script src="{{ asset('js/template.js')}}"></script>
    <script src="{{ asset('js/app.js')}}"></script>
</body>

</html>