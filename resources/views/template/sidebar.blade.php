<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            @if(Auth::user()->hasRole('users.list'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'users'}" :class="['nav-link']">
                    <i class="icon-info"></i>Usuarios<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
            @if(Auth::user()->hasRole('roles.list'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'roles'}" :class="['nav-link']">
                    <i class="icon-info"></i>Roles<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
            @if(Auth::user()->hasRole('portal.discount'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'portal.discount'}" :class="['nav-link']">
                    <i class="icon-info"></i>Descuentos Portal<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
            @if(Auth::user()->hasRole('taxes.list'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'taxes'}" :class="['nav-link']">
                    <i class="icon-info"></i>Impuestos<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
            @if(Auth::user()->hasRole('fields.list'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'fields'}" :class="['nav-link']">
                    <i class="icon-info"></i>Campos<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
            @if(Auth::user()->hasRole('groups.list'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'groups'}" :class="['nav-link']">
                    <i class="icon-info"></i>Grupos<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
            @if(Auth::user()->hasRole('products.list'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'products'}" :class="['nav-link']">
                    <i class="icon-info"></i>Productos<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
            @if(Auth::user()->hasRole('bonos.list'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'bonos'}" :class="['nav-link']">
                    <i class="icon-info"></i>Bonos<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif
            @if(Auth::user()->hasRole('log-user.list'))
            <li class="nav-item">
                <router-link exact-active-class="active" :to="{ name: 'log-user'}" :class="['nav-link']">
                    <i class="icon-info"></i>Log usuario<span class="badge badge-info"></span>
                </router-link>
            </li>
            @endif





        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>