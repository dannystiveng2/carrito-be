<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Informacion de pago jelpit</title>
    <link href="{{asset('css/template.css')}}" rel="stylesheet">
</head>
<style>
    body {
        paping: 0;
        margin: 0;
        font-family: Arial;
    }

    table,
    tr,
    td,
    th {
        border: 0;
        border-collapse: collapse;
        paping: 0;
        margin: 0;
    }
    /* button:focus {
        outline: none;
    } */
</style>
<body>
    <div>
        <p><strong>Estado:</strong> {{$order->status}}</p>
        <p><strong>Nombre:</strong> {{$order->personal_information->first_name}}</p>
        <p><strong>Apellido:</strong> {{$order->personal_information->last_name}}</p>
        <p><strong>Correo:</strong> {{$order->personal_information->email}}</p>
        <p><strong>Telefono:</strong>{{$order->personal_information->phone}}</p>
        <p><strong>Direccion:</strong> {{$order->personal_information->address}}</p>
        <p><strong>Fecha:</strong> {{$order->service_date}}</p>
        <p><strong>Hora:</strong> {{$order->service_hour}}</p>
        <p><strong>Servicio:</strong> {{$order->products()->first()->slug}}</p>
        <p><strong>Total:</strong> {{$order->total}}</p>
        <p><strong>Comprobante:</strong> {{$order->voucher}}</p>
        <p><strong>Codigo autorizacion:</strong> {{$order->authorization_code}}</p>
    </div>
</body>
</html>