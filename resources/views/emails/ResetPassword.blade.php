<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reestablecer contraseña</title>
</head>

<style>
    body {
        padding: 0;
        margin: 0;
        font-family: Helvetica;
    }

    table,
    tr,
    td,
    th {
        border: 0;
        border-collapse: collapse;
        padding: 0;
        margin: 0;
    }
</style>

<body>

    <table style="width: 100%;">
        <tr>
            <td>
                <table style="text-align: center; max-width: 650px; margin: 0 auto; background-color: #f5f5f5; overflow: hidden; border: 0; border-collapse: collapse; border-spacing: 0;" width="650">
                    <tr>
                        <td>
                            <img src="https://www.jelpit.com/assets/img/mailing/header-logo.jpg" style="display: block; margin:0 auto 0px; border:0;" alt="Jelpify" title="Jelpify">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="background-color: #fff; width: 100%; border: 0; border-collapse: collapse; border-spacing: 0;">
                                <tr>
                                    <td style="padding: 36px 24px 0;">
                                        <h1 style="color: #0a3c54; font-size: 35px; font-weight: bold; line-height: 42px; text-align: center; margin: 0; font-family: Helvetica;">{{$personal_information->first_name}} {{$personal_information->last_name }},</h1>
                                        <p style="color: #000000; font-size: 16px; line-height: 24px; text-align: center; margin: 5px 0; margin-bottom: 28px; font-family: Helvetica;" >¡Te damos la bienvenida a Jelpit!</p>
                                        <p style="color: #333; font-size: 16px; text-align: center; max-width: 550px; margin: 16px auto; line-height: 24px; margin-bottom: 36px; font-family: Helvetica;">¡A partir de ahora, podrás contar con nuestros servicios!</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; padding: 0 24px 48px;">   
                                        <span style="display: block; text-align: center; background-color: #4156c0; border: 1px solid #4156c0; border-radius: 36px; margin: 0 auto; width: 275px;">
                                            <span style="display: inline-block; background-color: #4156c0; border-radius: 36px;">
                                                <a style="display: inline-block; border-radius: 36px; text-decoration: none; color: #ffffff; border: 10px solid #4156c0; white-space: nowrap; font-size: 16px;  font-family: Arial;" href="{{ config('app.url_reset')}}/{{$password_reset->token}}">REESTABLECER CONTRASEÑA</a>
                                            </span>
                                        </span>
                                        <p style="color: #0a3c54; font-family: Helvetica; font-size: 16px; font-weight: bold; line-height: 17px; margin: 37px 0 0;">¡En Jelpit, te damos la mano con lo que necesites!</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: #1c313a;" >
                        <td style="display: block; padding: 24px 0; width: 100%;">
                                <a href="https://www.facebook.com/Doctor-Ak%C3%AD-858813907799520/" target="_blank" style=" width: 26px; height: 26px; display: inline-block; margin: 0 10px; background-color: transparent; border: 0; outline: none; cursor: pointer;"><img src="https://www.jelpit.com/assets/img/mailing/facebook.png" height="26" width="26"></a>
                                <a href="https://www.facebook.com/Doctor-Ak%C3%AD-858813907799520/" target="_blank" style=" width: 26px; height: 26px; display: inline-block; margin: 0 10px; background-color: transparent; border: 0; outline: none; cursor: pointer;"><img src="https://www.jelpit.com/assets/img/mailing/linkedin.png" height="26" width="26"></a>                                

                                <p style="margin: 2px; font-size: 10px; color: #fff; text-align: center; font-weight: 500; padding-top: 10px; font-family: Helvetica;" >Avenida el Dorado # 68B - 31 Bogotá Cundinamarca Colombia</p>
                                <p style="margin: 2px; font-size: 10px; color: #fff; text-align: center; font-weight: 100; font-family: Helvetica;" >Mensaje enviado de <a style="color: #fff;" href="mailto:comunicaciones@jelpify.com">comunicaciones@jelpit.com</a> a <a style="color: #fff;" href="mailto:info@jelpit.com">info@jelpit.com</a></p>
                                <p style="margin: 2px; font-size: 10px; color: #fff; text-align: center; font-weight: 100; font-family: Helvetica;" >Buscamos brindarle la mejor información, pero si quiere dejar de recibir estos mensaje haga <a href="#" style="color: #fff;">clic aquí</a></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</body>

</html>