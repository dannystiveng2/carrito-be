import Home from "@/components/ExampleComponent.vue";

import LogUser from "@/components/LogUser/LogUserComponent.vue";
import LogUserShow from "@/components/LogUser/LogUserShowComponent.vue";
import Products from "@/components/Product/ProductComponent.vue";
import ProductsForm from "@/components/Product/ProductFormComponent.vue";
import Fields from "@/components/Field/FieldComponent.vue";
import FieldsForm from "@/components/Field/FieldFormComponent.vue";

import Taxes from "@/components/Tax/TaxComponent.vue";
import TaxesForm from "@/components/Tax/TaxFormComponent.vue";

import Groups from "@/components/Group/GroupComponent.vue";
import GroupsForm from "@/components/Group/GroupFormComponent.vue";

import Bonos from "@/components/Bono/BonoComponent.vue";
import BonosForm from "@/components/Bono/BonoFormComponent.vue";

import DiscountForm from "@/components/Discount/DiscountFormComponent.vue";

import User from "@/components/User/UserComponent.vue";
import UserForm from "@/components/User/UserFormComponent.vue";

import Role from "@/components/Role/RoleComponent.vue";
import RoleForm from "@/components/Role/RoleFormComponent.vue";

export default [
    { path: '', name: 'index', component: Home },
    { path: '/log-user', name: 'log-user', component: LogUser },
    { path: '/log-user/show/:id', name: 'log-user-show', component: LogUserShow },

    { path: '/products', name: 'products', component: Products },
    { path: '/products/create', name: 'products.create', component: ProductsForm },
    { path: '/products/:id/edit', name: 'products.edit', component: ProductsForm },

    { path: '/fields', name: 'fields', component: Fields },
    { path: '/fields/create', name: 'fields.create', component: FieldsForm },
    { path: '/fields/:id/edit', name: 'fields.edit', component: FieldsForm },


    { path: '/taxes', name: 'taxes', component: Taxes },
    { path: '/taxes/create', name: 'taxes.create', component: TaxesForm },
    { path: '/taxes/:id/edit', name: 'taxes.edit', component: TaxesForm },

    { path: '/groups', name: 'groups', component: Groups },
    { path: '/groups/create', name: 'groups.create', component: GroupsForm },
    { path: '/groups/:id/edit', name: 'groups.edit', component: GroupsForm },

    { path: '/bonos', name: 'bonos', component: Bonos },
    { path: '/bonos/create', name: 'bonos.create', component: BonosForm },
    { path: '/bonos/:id/edit', name: 'bonos.edit', component: BonosForm },

    { path: '/portal/discount', name: 'portal.discount', component: DiscountForm },

    { path: '/users', name: 'users', component: User },
    { path: '/users/:type/:id?', name: 'user.form', component: UserForm },
    { path: '/roles', name: 'roles', component: Role },
    { path: '/roles/:type/:id?', name: 'role.form', component: RoleForm },

    { path: '**', name: 'all', component: Home },
];