/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import draggable from 'vuedraggable';
import Multiselect from 'vue-multiselect';
import DatePicker from 'vue2-datepicker';
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import FileUpload from "@/components/FilesUpload/FileUpload.vue";
import VueQuillEditor from 'vue-quill-editor'
Vue.use(require('vue-moment'));




Vue.use(VueRouter);
Vue.use(Antd);

// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.use(VueQuillEditor)

Vue.config.devtools = true;


Vue.component('multiselect', Multiselect);
Vue.component('draggable', draggable);
Vue.component('FileUpload', FileUpload);
Vue.component('date-picker', DatePicker);
Vue.component('template-uploader', require('@/components/TemplateUploaderComponent.vue').default);
Vue.component('multiselect', Multiselect);

import routes from "@/router";
const router = new VueRouter({
    routes: routes
});


const app = new Vue({
    el: '#app',
    router,
    data() {
        return {
            permissions: []
        }
    },
    beforeMount() {
        let _this = this;
        axios.get("/admin/user-permission").then(res => {
            _this.permissions = res.data;
        });
    },
    created() {


        this.$message.config({
            top: "80px",
            duration: 2,
            maxCount: 3
        });
    }
});